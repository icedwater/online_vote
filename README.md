# Online Voting

Designed for the clan association I'm in.

## Segments

1. generate candidate profile pages
2. generate member voting forms
3. collate and validate votes
4. generate statistics reports
5. generate member notifications
6. 
## Generate Member Pages

- Read from database, show page
- Using CSV as dummy

## Generate Member Voting Form

- Define $target committee size
- Each member has $target votes

## Collate and Validate Votes

- Make sure no duplicates within vote form
- Count votes for candidates

## Generate Statistics Reports

- Vote totals per candidate

## Generate Member Notifications

- Inform elected committee
